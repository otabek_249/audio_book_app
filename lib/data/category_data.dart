
import 'dart:ui';

class CategoryData {
  String name;
  Color color;
  Color textColor;
  CategoryData({ required this.color, required this.textColor, required this.name});
}