
import 'package:firebase_auth/firebase_auth.dart';

class AuthRepository{
  var firebaseAuth = FirebaseAuth.instance;
  void signIn({required String email,required String password }){
    try{
      firebaseAuth.createUserWithEmailAndPassword(email: email, password: password);
    }catch(e){
      throw("Error:$e");
    }
  }


  void logIn({required String email,required String password }){
    try{
      firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
    }catch(e){
      throw("Error:$e");
    }
  }
}