import 'package:audio_book_app/ui/auth/login/bloc/login_bloc.dart';
import 'package:audio_book_app/ui/auth/login/login_screen.dart';
import 'package:audio_book_app/ui/auth/sign_up/bloc/sign_up_bloc.dart';
import 'package:audio_book_app/ui/auth/sign_up/sign_up_screen.dart';
import 'package:audio_book_app/ui/main/home/bloc/home_bloc.dart';
import 'package:audio_book_app/ui/main/home/home_screen.dart';
import 'package:audio_book_app/ui/main/home/pages/main/bloc/main_bloc.dart';
import 'package:audio_book_app/ui/main/home/pages/main/main_screen.dart';
import 'package:audio_book_app/ui/main/home/pages/profile/profile_screen.dart';
import 'package:audio_book_app/ui/main/home/pages/search/search_screen.dart';
import 'package:audio_book_app/ui/main/music/music_screen.dart';
import 'package:audio_book_app/ui/main/splash/splash_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // TRY THIS: Try running your application with "flutter run". You'll see
        // the application has a purple toolbar. Then, without quitting the app,
        // try changing the seedColor in the colorScheme below to Colors.green
        // and then invoke "hot reload" (save your changes or press the "hot
        // reload" button in a Flutter-supported IDE, or press "r" if you used
        // the command line to start the app).
        //
        // Notice that the counter didn't reset back to zero; the application
        // state is not lost during the reload. To reset the state, use hot
        // restart instead.
        //
        // This works for code too, not just values: Most code changes can be
        // tested with just a hot reload.
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: BlocProvider(
        create: (context) => LoginBloc(),
        child: const LoginScreen(),
      ),
      initialRoute: '/login',
      routes: {
        '/main': (context) =>
            MultiBlocProvider(
              providers: [
                BlocProvider(create: (context) => MainBloc()),
              ],
              child: const MainScreen(),
            ),
        '/splash': (context) => const SplashScreen(),
        '/register': (context) =>
            BlocProvider(
              create: (context) => SignUpBloc(),
              child: const SignUpScreen(),
            ),
        '/login': (context) =>
            BlocProvider(
              create: (context) => LoginBloc(),
              child: const LoginScreen(),
            ),
        '/home': (context) =>
            BlocProvider(
              create: (context) => HomeBloc(),
              child: const HomeScreen(),
            ),

        '/play': (context) => const MusicScreen(),
        '/profile': (context) => const ProfileScreen(),
        '/search': (context) => const SearchScreen(),
      },
    );
  }
}


