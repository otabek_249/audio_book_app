import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

import 'bloc/sign_up_bloc.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({super.key});

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  String email ="";
  String password ="";
  @override
  Widget build(BuildContext context) {
    return BlocListener<SignUpBloc, SignUpState>(
  listener: (context, state) {
    if(state.moveToLogin??false){
      Navigator.pushReplacementNamed(context, '/login');
    }
    else if(state.moveToHome??false){
      Navigator.pushReplacementNamed(context, '/home');
    }
  },
  child: Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 33),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 7,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 53),
                        child: Row(
                          children: [
                            const Icon(
                              Icons.arrow_back,
                              color: Color(0xff787878),
                            ),
                            const SizedBox(
                              width: 9,
                            ),
                            Text(
                              "Go Back",
                              style: GoogleFonts.roboto(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w700,
                                  color: const Color(0xff787878)),
                            )
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 47,
                      ),
                      Image.asset(
                        "assets/images/ic_audio_book.png",
                        height: 100,
                        width: 100,
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Text(
                        "Create an Account",
                        style: GoogleFonts.roboto(
                            fontWeight: FontWeight.w700,
                            fontSize: 24,
                            color: Colors.black),
                      ),
                      const SizedBox(
                        height: 13,
                      ),
                      Text(
                        "Register to continue",
                        style: GoogleFonts.roboto(
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                            color: const Color(0xff787878)),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 10,
                    child: Column(
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      height: 56,
                      child: TextField(
                        onChanged: (text) {
                          email = text;
                        },
                        decoration: InputDecoration(
                            hintText: "Email Address",
                            hintStyle: GoogleFonts.roboto(
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                                color: const Color(0xff787878))),
                      ),
                    ),
                    const SizedBox(height: 8),
                    SizedBox(
                      height: 56,
                      child: TextField(
                        onChanged: (text) {
                          password = text;
                        },
                        decoration: InputDecoration(
                            hintText: "Password",
                            hintStyle: GoogleFonts.roboto(
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                                color: const Color(0xff787878))),
                      ),
                    ),
                    const SizedBox(height: 8),
                    SizedBox(
                      height: 56,
                      child: TextField(
                        onChanged: (text) {},
                        decoration: InputDecoration(
                            hintText: "Retype password",
                            hintStyle: GoogleFonts.roboto(
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                                color: const Color(0xff787878))),
                      ),
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                    GestureDetector(
                      onTap: (){
                        context.read<SignUpBloc>().add(CreateAccount(email: email, password: password));
                      },
                      child: Container(
                        margin: const EdgeInsets.symmetric(horizontal: 85),
                        height: 43,
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: const Color(0xfff26b6c),
                            borderRadius: BorderRadius.circular(20)),
                        child: Center(
                          child: Text(
                            "Register",
                            style: GoogleFonts.roboto(
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                                color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Center(
                      child: Text(
                        "or register using ",
                        style: GoogleFonts.roboto(
                            fontWeight: FontWeight.w500,
                            fontSize: 14,
                            color: const Color(0xff787878)),
                      ),
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    Row(
                      children: [
                        const Spacer(),
                        Image.asset(
                          "assets/images/ic_google.png",
                          height: 36,
                          width: 36,
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Image.asset(
                          "assets/images/ic_facebook.png",
                          height: 36,
                          width: 36,
                        ),
                        const Spacer(),
                      ],
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Row(
                      children: [
                        const Spacer(),
                        Text(
                          "Already have an account?",
                          style: GoogleFonts.roboto(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        GestureDetector(
                          onTap: (){
                            context.read<SignUpBloc>().add(SignMoveToLogin());
                          },
                          child: Text(
                            "Sign In?",
                            style: GoogleFonts.roboto(
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                color: const Color(0xFFD71920)),
                          ),
                        ),
                        const Spacer(),
                      ],

                    ),
                    const SizedBox(height: 30,)
                  ],
                ))
              ],
            ),
          ),
        ],
      ),
    ),
);
  }
}
