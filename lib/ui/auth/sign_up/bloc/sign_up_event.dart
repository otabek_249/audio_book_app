part of 'sign_up_bloc.dart';

@immutable
sealed class SignUpEvent {}


final class CreateAccount extends SignUpEvent{
  final String email;
  final String password;
  CreateAccount({required this.email, required this.password});
}


final class SignMoveToLogin extends SignUpEvent{}


final class SignUpClickGoogleIcon extends SignUpEvent{}

final class SignUpClickFacebookIcon extends SignUpEvent{}