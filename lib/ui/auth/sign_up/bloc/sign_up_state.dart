part of 'sign_up_bloc.dart';

class SignUpState {
  bool? moveToHome;
  bool? moveToLogin;
  bool?clickGoogle;
  bool? clickFacebook;
  String ?  error;

  SignUpState({ this.clickGoogle, this.clickFacebook, this.moveToLogin, this.moveToHome, this.error});

  SignUpState copyWith({bool? moveToHome,bool? moveToLogin, bool?clickGoogle, bool? clickFacebook, String ?  error})=>
      SignUpState(moveToHome: moveToHome??this.moveToHome, moveToLogin: moveToLogin??this.moveToLogin, clickFacebook: clickFacebook??this.clickFacebook,
      clickGoogle: clickGoogle??this.clickGoogle,error: error??this.error);
}


