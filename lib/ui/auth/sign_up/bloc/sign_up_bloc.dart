import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../domain/auth_repository.dart';

part 'sign_up_event.dart';
part 'sign_up_state.dart';

class SignUpBloc extends Bloc<SignUpEvent, SignUpState> {
  var authRepo = AuthRepository();
  SignUpBloc() : super(SignUpState()) {
    on<SignMoveToLogin>((event, emit) {
      emit(SignUpState(moveToLogin: true));
    });
    on<CreateAccount>((event, emit) {
      try{
        authRepo.signIn(email: event.email, password:event.password);
        emit(SignUpState(moveToHome: true));
      }
      catch(e){
        emit(SignUpState(moveToHome :false,error: e.toString()));
      }

    });
    on<SignUpClickGoogleIcon>((event, emit) {
      emit(SignUpState(clickGoogle: true));
    });
    on<SignUpClickFacebookIcon>((event, emit) {
      emit(SignUpState(clickFacebook: true));
    });


  }
}
