part of 'login_bloc.dart';

@immutable
sealed class LoginEvent {}



final class LoginButtonClick extends LoginEvent{
  final String email;
  final String passowrd;
  LoginButtonClick({required this.email, required this.passowrd});
}

final class LoginMoveToSignUp extends LoginEvent{}

final class LoginClickGoogleIcon extends LoginEvent{}

final class LoginClickFacebookIcon extends LoginEvent{}
