part of 'login_bloc.dart';

 class LoginState {
  bool? moveToHome;
  bool? moveToSignUp;
  bool?clickGoogle;
  bool? clickFacebook;
  String? error;

  LoginState({ this.error,this.clickFacebook, this.clickGoogle, this.moveToHome, this.moveToSignUp});

  LoginState copyWith({ bool? moveToHome,bool? moveToSignUp,bool?clickGoogle,bool? clickFacebook, String?error}) =>
      LoginState(moveToHome: moveToHome ?? this.moveToHome, moveToSignUp: moveToSignUp ?? this.moveToSignUp,
       clickFacebook: clickFacebook??this.clickFacebook,clickGoogle: clickGoogle??this.clickGoogle,error: error??this.error,
      );




}


