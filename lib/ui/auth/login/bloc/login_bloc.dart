import 'package:audio_book_app/domain/auth_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  var authRepo = AuthRepository();
  LoginBloc() : super(LoginState()) {
    on<LoginClickGoogleIcon>((event, emit) {
      emit(LoginState(clickGoogle: true));
    });
    on<LoginButtonClick>((event, emit) {
      try{
        authRepo.logIn(email: event.email, password: event.passowrd);
        emit(LoginState(moveToHome: true));
      }catch(e){
        emit(LoginState(moveToHome: false,error: e.toString()));

      }
    });
    on<LoginMoveToSignUp>((event, emit) {
      emit(LoginState(moveToSignUp: true));
    });
    on<LoginClickFacebookIcon>((event, emit) {
      emit(LoginState(clickFacebook: true));
    });
  }
}
