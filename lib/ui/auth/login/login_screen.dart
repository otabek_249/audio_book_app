import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

import 'bloc/login_bloc.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String email = "";
  String password = "";

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state.moveToHome ?? false) {
          Navigator.pushReplacementNamed(
            context,
            '/home',
          );
        }
        else if(state.moveToSignUp??false){
          Navigator.pushReplacementNamed(context, '/register');
        }
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 33),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 7,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 53),
                          child: Row(
                            children: [
                              const Icon(
                                Icons.arrow_back,
                                color: Color(0xff787878),
                              ),
                              const SizedBox(
                                width: 9,
                              ),
                              Text(
                                "Go Back",
                                style: GoogleFonts.roboto(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w700,
                                    color: const Color(0xff787878)),
                              )
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 47,
                        ),
                        Image.asset(
                          "assets/images/ic_audio_book.png",
                          height: 100,
                          width: 100,
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        Text(
                          "Welcome Back",
                          style: GoogleFonts.roboto(
                              fontWeight: FontWeight.w700,
                              fontSize: 24,
                              color: Colors.black),
                        ),
                        const SizedBox(
                          height: 13,
                        ),
                        Text(
                          "Sign In to continue",
                          style: GoogleFonts.roboto(
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                              color: const Color(0xff787878)),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                      flex: 10,
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 20,
                          ),
                          SizedBox(
                            height: 56,
                            child: TextField(
                              onChanged: (text) {
                                email = text;
                              },
                              decoration: InputDecoration(
                                  hintText: "Email Address",
                                  hintStyle: GoogleFonts.roboto(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14,
                                      color: const Color(0xff787878))),
                            ),
                          ),
                          const SizedBox(height: 8),
                          SizedBox(
                            height: 56,
                            child: TextField(
                              onChanged: (text) {
                                password = text;
                              },
                              decoration: InputDecoration(
                                  hintText: "Password",
                                  hintStyle: GoogleFonts.roboto(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14,
                                      color: const Color(0xff787878))),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: Text(
                              "Forgot Password",
                              style: GoogleFonts.roboto(
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                                color: const Color(0xFFD71920),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 40,
                          ),
                          GestureDetector(
                            onTap: () {
                              context.read<LoginBloc>().add(LoginButtonClick(
                                  email: email, passowrd: password));
                            },
                            child: Container(
                              margin:
                                  const EdgeInsets.symmetric(horizontal: 85),
                              height: 43,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  color: const Color(0xfff26b6c),
                                  borderRadius: BorderRadius.circular(20)),
                              child: Center(
                                child: Text(
                                  "Register",
                                  style: GoogleFonts.roboto(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          Center(
                            child: Text(
                              "or sign in using",
                              style: GoogleFonts.roboto(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                  color: const Color(0xff787878)),
                            ),
                          ),
                          const SizedBox(
                            height: 12,
                          ),
                          Row(
                            children: [
                              const Spacer(),
                              Image.asset(
                                "assets/images/ic_google.png",
                                height: 36,
                                width: 36,
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              Image.asset(
                                "assets/images/ic_facebook.png",
                                height: 36,
                                width: 36,
                              ),
                              const Spacer(),
                            ],
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          Row(
                            children: [
                              const Spacer(),
                              Text(
                                "Don’t have an account?",
                                style: GoogleFonts.roboto(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              GestureDetector(
                                onTap: () {
                                  context
                                      .read<LoginBloc>()
                                      .add(LoginMoveToSignUp());
                                },
                                child: Text(
                                  "Register?",
                                  style: GoogleFonts.roboto(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      color: const Color(0xFFD71920)),
                                ),
                              ),
                              const Spacer(),
                            ],
                          ),
                          const SizedBox(
                            height: 30,
                          )
                        ],
                      ))
                ],
              ),
            ),
          ],
        ),
      ),
    );
    ;
  }
}
