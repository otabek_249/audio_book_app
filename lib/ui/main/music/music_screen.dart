import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MusicScreen extends StatefulWidget {
  const MusicScreen({super.key});

  @override
  State<MusicScreen> createState() => _MusicScreenState();
}

class _MusicScreenState extends State<MusicScreen> {
  @override
  Widget build(BuildContext context) {
    double customW = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: const Color(0xfff26b6c),
        leading: const Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
        title: Center(
          child: Text(
            "Now Playing",
            style: GoogleFonts.poppins(
                fontWeight: FontWeight.w700, fontSize: 20, color: Colors.white),
          ),
        ),
        actions: const [
          Icon(
            Icons.more_vert,
            color: Colors.white,
          ),
          SizedBox(
            width: 16,
          )
        ],
      ),
      body: Stack(
        children: [
          Positioned(
            child: Container(
              height: 150,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.vertical(
                  bottom: Radius.elliptical(200, 150),
                ),
                color: Color(0xfff26b6c),
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                height: 30,
              ),
              Center(
                  child: Image.asset(
                "assets/images/book2.png",
                height: 274,
                width: 200,
                fit: BoxFit.fill,
              )),
              Text("MR. MERCEDES", style: GoogleFonts.poppins(
                fontSize: 14,
                fontWeight: FontWeight.w500,
                color: const Color(0xff787878)
              ),)
            ],
          ),
        ],
      ),
    );
  }
}
