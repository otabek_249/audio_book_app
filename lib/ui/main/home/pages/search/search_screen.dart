import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({super.key});

  @override
  State<SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 24),
        child: Column(
          children: [
            TextField(
              decoration: InputDecoration(
                  prefixIcon: const Icon(
                    Icons.arrow_back,
                    color: Colors.grey,
                  ),
                  hintText: "search books",
                  hintStyle: GoogleFonts.poppins(
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    color: Colors.grey
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide:
                      const BorderSide(color: Colors.redAccent, width: 2)
                  ),
                  enabledBorder: OutlineInputBorder(

                    borderRadius: BorderRadius.circular(5),
                      borderSide:
                          const BorderSide(color: Colors.redAccent, width: 2))),
            )
          ],
        ),
      ),
    );
  }
}
