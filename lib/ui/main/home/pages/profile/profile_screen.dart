import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {

  double textSize= 15;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: const Color(0xFFF26B6C),
        leading: const Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
        title: Center(
          child: Text(
            "Profile",
            style: GoogleFonts.roboto(
                fontSize: 20, fontWeight: FontWeight.w700, color: Colors.white),
          ),
        ),
        actions: const [
          Icon(
            Icons.edit_calendar_outlined,
            color: Colors.white,
          ),
          SizedBox(
            width: 16,
          )
        ],
      ),
      body: Column(
        children: [
          Expanded(
              flex: 2,
              child: Stack(
                children: [
                  Container(
                    decoration: const BoxDecoration(
                        color: Color(0xFFF26B6C),
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(15),
                            bottomRight: Radius.circular(15))),
                  ),
                  Center(
                    child: Column(
                      children: [
                        const Spacer(),
                        Image.asset(
                          "assets/images/opa.png",
                          width: 122,
                          height: 122,
                        ),
                        const SizedBox(
                          height: 14,
                        ),
                        Text(
                          "Maria Akter Dipti",
                          style: GoogleFonts.roboto(
                              fontWeight: FontWeight.w700,
                              fontSize: 24,
                              color: Colors.white),
                        ),
                        const Spacer(),
                      ],
                    ),
                  )
                ],
              )),
          Expanded(
              flex: 3,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Column(
                  children: [
                    const SizedBox(height: 20,),
                    Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                            flex: 6,
                                child: Text(
                              "User Name",
                              style: GoogleFonts.roboto(
                                  fontSize: textSize,
                                  fontWeight: FontWeight.w700,
                                  color: const Color(0xfff26b6c)),
                            )),
                            Expanded(
                              flex: 5,
                                child: Text(
                              "dipti_2020",
                              style: GoogleFonts.roboto(
                                  fontSize: textSize,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.grey),
                            ))
                          ],
                        ),
                         const SizedBox(height: 2,),
                        const Divider(height: 1,color: Colors.grey,)
                      ],
                    ),
                    const SizedBox(height: 20,),
                    Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                                flex: 6,
                                child: Text(
                                  "Email",
                                  style: GoogleFonts.roboto(
                                      fontSize: textSize,
                                      fontWeight: FontWeight.w700,
                                      color: const Color(0xfff26b6c)),
                                )),
                            Expanded(
                                flex: 5,
                                child: Text(
                                  "mariadipti@gmail.com",
                                  style: GoogleFonts.roboto(
                                      fontSize: textSize,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.grey),
                                ))
                          ],
                        ),
                        const SizedBox(height: 2,),
                        const Divider(height: 1,color: Colors.grey,)
                      ],
                    ),
                    const SizedBox(height: 20,),
                    Column(
                      children: [
                        Row(
                          children: [
                            Text(
                              "Change Password",
                              style: GoogleFonts.roboto(
                                  fontSize: textSize,
                                  fontWeight: FontWeight.w700,
                                  color: const Color(0xfff26b6c)),
                            ),
                            const Spacer(),
                            Icon(Icons.arrow_forward_ios, color: Colors.grey,size: textSize,)
                          ],
                        ),
                        const SizedBox(height: 2,),
                        const Divider(height: 1,color: Colors.grey,)
                      ],
                    ),
                    const SizedBox(height: 20,),
                    Column(
                      children: [
                        Row(
                          children: [
                            Text(
                              "Notification",
                              style: GoogleFonts.roboto(
                                  fontSize: textSize,
                                  fontWeight: FontWeight.w700,
                                  color: const Color(0xfff26b6c)),
                            ),
                            const Spacer(),
                            const Icon(Icons.notifications, color: Colors.grey,size: 18,)
                          ],
                        ),
                        const SizedBox(height: 2,),
                        const Divider(height: 1,color: Colors.grey,)
                      ],
                    ),
                    const SizedBox(height: 20,),
                    Column(
                      children: [
                        Row(
                          children: [
                            Text(
                              "Enable Dark Mode",
                              style: GoogleFonts.roboto(
                                  fontSize: textSize,
                                  fontWeight: FontWeight.w700,
                                  color: const Color(0xfff26b6c)),
                            ),
                            const Spacer(),
                            Icon(Icons.arrow_forward_ios, color: Colors.grey,size: 18,)
                          ],
                        ),
                        const SizedBox(height: 2,),
                        const Divider(height: 1,color: Colors.grey,)
                      ],
                    ),
                    const SizedBox(height: 20,),
                    Column(
                      children: [
                        Row(
                          children: [
                            Text(
                              "Settings",
                              style: GoogleFonts.roboto(
                                  fontSize: textSize,
                                  fontWeight: FontWeight.w700,
                                  color: const Color(0xfff26b6c)),
                            ),
                            const Spacer(),
                            Icon(Icons.settings, color: Colors.grey,size: 18,)
                          ],
                        ),
                        const SizedBox(height: 2,),
                        const Divider(height: 1,color: Colors.grey,)
                      ],
                    ),


                  ],
                ),
              ))
        ],
      ),
    );
  }
}

Widget _item() {
  return Container();
}
