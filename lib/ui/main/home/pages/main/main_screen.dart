import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../../../data/category_data.dart';
import '../../component/book_item.dart';
class MainScreen extends StatefulWidget {
  const MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        forceMaterialTransparency: true,
        backgroundColor: Colors.transparent,
        leading: const Icon(Icons.arrow_back),
        title: Center(
          child: Text(
            "Explore",
            style: GoogleFonts.roboto(
                color: const Color(0xFFF26B6C),
                fontWeight: FontWeight.w700,
                fontSize: 24),
          ),
        ),
        actions: const [
          Icon(Icons.more_vert),
          SizedBox(
            width: 16,
          )
        ],
      ),
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              itemTop(
                  "Thriller", const Color(0xfffef1f1), const Color(0xfff47c7d)),
              itemTop(
                  "Suspense", const Color(0xfff1fbf5), const Color(0xff219653)),
              itemTop(
                  "Humour", const Color(0xfffefaee), const Color(0xfff2c94c)),
            ],
          ),
          Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 17, vertical: 4),
                child: GridView.builder(
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3, // Number of columns in the grid
                      crossAxisSpacing: 5.0,
                      mainAxisSpacing: 10.0,
                      childAspectRatio: .66,
                    ),
                    itemCount: 20,
                    itemBuilder: (context, index) {
                      return SizedBox(
                        height: 200,
                        child: BookItem(
                            path: "assets/images/book.png", onClick: () {}),
                      );
                    }),
              ))
        ],
      ),
    );
  }
}


final List<CategoryData> listCategory = [
  CategoryData(
      color: const Color(0xfffef1f1),
      textColor: const Color(0xfff47c7d),
      name: "Thriller"),
  CategoryData(
      color: const Color(0xfff1fbf5),
      textColor: const Color(0xff219653),
      name: "Thriller"),
  CategoryData(
      color: const Color(0xfffefaee),
      textColor: const Color(0xfff2c94c),
      name: "Thriller"),
];

Widget itemTop(String name, Color color, Color textColor) {
  return Container(
    height: 50,
    width: 100,
    decoration:
    BoxDecoration(color: color, borderRadius: BorderRadius.circular(12)),
    child: Center(
      child: Text(
        name,
        style: GoogleFonts.roboto(
            color: textColor, fontSize: 12, fontWeight: FontWeight.w700),
      ),
    ),
  );
}
