
import 'package:audio_book_app/ui/main/home/pages/library/bloc/library_bloc.dart';
import 'package:audio_book_app/ui/main/home/pages/library/library_screen.dart';
import 'package:audio_book_app/ui/main/home/pages/main/bloc/main_bloc.dart';
import 'package:audio_book_app/ui/main/home/pages/main/main_screen.dart';
import 'package:audio_book_app/ui/main/home/pages/profile/bloc/profile_bloc.dart';
import 'package:audio_book_app/ui/main/home/pages/profile/profile_screen.dart';
import 'package:audio_book_app/ui/main/home/pages/search/bloc/search_bloc.dart';
import 'package:audio_book_app/ui/main/home/pages/search/search_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/home_bloc.dart';


class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomeBloc, HomeState>(
  listener: (context, state) {

  },
  builder: (context, state) {
    return Scaffold(
      backgroundColor: Colors.white,
      extendBody: true,
      body: SafeArea(
        child: IndexedStack(
          index: state.selected,
          children: <Widget>[
            BlocProvider(
              create: (context) => MainBloc(),
              child: const MainScreen(),
            ),
            BlocProvider(
              create: (context) => SearchBloc(),
              child: const SearchScreen(),
            ),
            BlocProvider(
              create: (context) => LibraryBloc(),
              child: const LibraryScreen(),
            ),
            BlocProvider(
              create: (context) => ProfileBloc(),
              child: const ProfileScreen(),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50),
        ),
        backgroundColor: const Color(0xFFFFFFFF),
        onPressed: () {
          Navigator.pushNamed(context, '/play');
        },
        child: Container(
          width: double.maxFinite,
          height: double.maxFinite,
          margin: const EdgeInsets.all(3),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            color: const Color(0xFFF26B6C),
          ),
          child: const Icon(
            Icons.play_arrow,
            size: 36,
            color: Color(0xFFFFFFFF),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        color: Colors.white,
        height: 72,
        shape: const CircularNotchedRectangle(),
        notchMargin: 5,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  context.read<HomeBloc>().add(SelectItem(item: 0));
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.home,
                      color: state.selected == 0 ? const Color(0xFFF26B6C) : const Color(0xFF7D7D7D),
                      size: 28,
                    ),
                    Text(
                        "Home",
                      style: TextStyle(
                        color: state.selected == 0 ? const Color(0xFFF26B6C) : const Color(0xFF7D7D7D),
                        fontSize: 12,
                        fontWeight: FontWeight.w500
                      ),
                    )
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  context.read<HomeBloc>().add(SelectItem(item: 1));
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.search,
                      color: state.selected == 1 ? const Color(0xFFF26B6C) : const Color(0xFF7D7D7D),
                      size: 28,
                    ),
                    Text(
                      "Search",
                      style: TextStyle(
                          color: state.selected == 1 ? const Color(0xFFF26B6C) : const Color(0xFF7D7D7D),
                          fontSize: 12,
                          fontWeight: FontWeight.w500
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(width: 0),
              GestureDetector(
                onTap: () {
                  context.read<HomeBloc>().add(SelectItem(item: 2));
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.library_books,
                      color: state.selected == 2 ? const Color(0xFFF26B6C) : const Color(0xFF7D7D7D),
                      size: 28,
                    ),
                    Text(
                      "Library",
                      style: TextStyle(
                          color: state.selected == 2 ? const Color(0xFFF26B6C) : const Color(0xFF7D7D7D),
                          fontSize: 12,
                          fontWeight: FontWeight.w500
                      ),
                    )
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  context.read<HomeBloc>().add(SelectItem(item: 3));
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.person,
                      color: state.selected == 3 ? const Color(0xFFF26B6C) : const Color(0xFF7D7D7D),
                      size: 28,
                    ),
                    Text(
                      "Profile",
                      style: TextStyle(
                          color: state.selected == 3 ? const Color(0xFFF26B6C) : const Color(0xFF7D7D7D),
                          fontSize: 12,
                          fontWeight: FontWeight.w500
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  },
);
  }
}

