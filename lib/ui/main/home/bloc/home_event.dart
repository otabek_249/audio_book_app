part of 'home_bloc.dart';

sealed class HomeEvent {}

class SelectItem extends HomeEvent {
  int item;

  SelectItem({
    required this.item
  });
}
