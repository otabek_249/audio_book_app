part of 'home_bloc.dart';

class HomeState {
  int? selected;

  HomeState({
    required this.selected
  });

  HomeState copyOf({int? selected}) {
    return HomeState(
      selected: selected ?? this.selected
    );
  }
}



